package com.planetorora.utility;

import android.content.Context;
import android.graphics.Typeface;

import com.planetorora.base.AppConstants;

import java.util.Hashtable;

/**
 * Created by berjde on 2015. 12. 21..
 */
public class FontHelper {
    private static final Hashtable<String, Typeface> cache = new Hashtable<String, Typeface>();
    private static FontHelper ourInstance = new FontHelper();

    public static FontHelper getInstance() {
        return ourInstance;
    }

    private FontHelper() {
    }

    public static Typeface getFont(Context c, String assetPath) {
        synchronized (cache) {

            if (!cache.containsKey(assetPath)) {

                try {
                    Typeface t = (Typeface.createFromAsset(c.getAssets(),
                            AppConstants.FONT_REGULAR_KR));

                    cache.put(assetPath, t);

                } catch (Exception e) {
                    return null;
                }
            }
            return cache.get(assetPath);
        }
    }
}
