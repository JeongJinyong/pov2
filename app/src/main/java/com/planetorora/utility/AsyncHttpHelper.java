package com.planetorora.utility;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.SyncHttpClient;
import com.planetorora.base.AppConstants;

/**
 * Created by berjde on 2016. 1. 2..
 */
public class AsyncHttpHelper {

    private static AsyncHttpClient asyncClient = new AsyncHttpClient();

    public static void get(String userId, String userKey, String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        setUserKey(userId, userKey);
        asyncClient.get(getAbsoluteUrl(url), params, responseHandler);
    }
    public static void get(boolean async, String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        asyncClient.get(getAbsoluteUrl(url), params, responseHandler);
    }

    public static void post( String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        asyncClient.post(getAbsoluteUrl(url), params, responseHandler);
    }
    public static void post( String userId, String userKey, String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        setUserKey(userId, userKey);
        asyncClient.post(getAbsoluteUrl(url), params, responseHandler);
    }


    public static void put(boolean async, String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        asyncClient.put(getAbsoluteUrl(url), params, responseHandler);
    }

    public static void delete(boolean async, String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        asyncClient.delete(getAbsoluteUrl(url), params, responseHandler);
    }


    private static String getAbsoluteUrl(String relativeUrl) {
        return AppConstants.API_URL + relativeUrl;
    }
    private static void setUserKey(String userId, String userKey) {
        if(userId!=null && userKey!= null) {
            try {
                asyncClient.addHeader("User-Id", userId);
                asyncClient.addHeader("User-Key", userKey);
            }
            catch(Exception ex){
            }
        }
    }
}
