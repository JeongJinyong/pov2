package com.planetorora.utility;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by berjde on 2016. 1. 4..
 */
public class StringHelper {
    private static StringHelper helper =  new StringHelper();
    private StringHelper(){}

    public static StringHelper getInstance(){
        return helper;
    }

    public List<String> findTag(String content){
        Pattern MY_PATTERN = Pattern.compile("#(\\w+|\\W+)");
        Matcher mat = MY_PATTERN.matcher(content);
        List<String> list =new ArrayList<String>();
        while (mat.find()) {
           list.add(mat.group(1));
        }

        return list;
    }
}
