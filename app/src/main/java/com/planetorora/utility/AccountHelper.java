package com.planetorora.utility;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.os.Bundle;

import com.planetorora.base.AppConstants;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by berjde on 2015. 12. 19..
 */
public class AccountHelper {

    private  AccountHelper(){}
    private static AccountManager am;

    private static AccountManager getAccountManager(Context context){
        if( am == null)
            am = (AccountManager) context.getSystemService(context.ACCOUNT_SERVICE);

        return am;
    }

    public static JSONObject getUserInfo(Context context) {
        Account[] accounts = getAccountManager(context).getAccountsByType(AppConstants.ACCOUNT_TYPE);
        JSONObject result = null;
        if(accounts != null && accounts.length >0){
            try {
                String password = getAccountManager(context).getPassword(accounts[0]);
                String userInfo = getAccountManager(context).getUserData(accounts[0], AppConstants.USER_INFO);

                result = new JSONObject(getAccountManager(context).getUserData(accounts[0], AppConstants.USER_DATA));
                result.put("password", password);
                result.put("email", accounts[0].name);

                if(userInfo !=null && !userInfo.equals(""))
                    result.put("data", new JSONObject(userInfo));
            }
            catch(JSONException ex){}
        }
        return result;
    }

    private static Account getAccount(Context context){
        Account[] accounts = getAccountManager(context).getAccountsByType(AppConstants.ACCOUNT_TYPE);
        if(accounts != null && accounts.length >0) return accounts[0];
        else return null;

    }


    public static void setData(Context context, String name, String obj){
        Account account = getAccount(context);

        if(account != null){
            getAccountManager(context).setUserData(account, name, obj);
        }

    }

    public static boolean save(Context context, String userID, String password, JSONObject user_info){
        Account account = new Account(userID, AppConstants.ACCOUNT_TYPE);
        Bundle bundle = new Bundle();
        bundle.putString(AppConstants.USER_DATA, user_info.toString());

        if(getAccountManager(context).addAccountExplicitly(account, password, bundle)) {
            return true;
        }
        else{
            return false;
        }

    }

    public static void delete(Context context){
        Account account = getAccount(context);
        if(account != null) {
            getAccountManager(context).removeAccount(account, null, null);
        }
    }
}