package com.planetorora.utility;

import android.app.Activity;
import android.widget.Toast;

/**
 * Created by berjde on 2016. 1. 2..
 */
public class ExitHelper {

    private long backKeyPressedTime = 0;
    private Toast toast;
    private String message = null;
    private Activity activity;

    public ExitHelper(Activity context) {
        this.activity = context;
        this.message = "\'뒤로\'버튼을 한번 더 누르시면 종료됩니다.";

    }

    public ExitHelper(Activity context, String message) {
        this.activity = context;
        this.message = message;

    }


    public void onBackPressed() {
        if (System.currentTimeMillis() > backKeyPressedTime + 2000) {
            backKeyPressedTime = System.currentTimeMillis();
            showGuide();
            return;
        }
        if (System.currentTimeMillis() <= backKeyPressedTime + 2000) {
            activity.finish();
            toast.cancel();
        }
    }

    public void showGuide() {
        toast = Toast.makeText(activity,
                this.message, Toast.LENGTH_LONG);
        toast.show();
    }
}