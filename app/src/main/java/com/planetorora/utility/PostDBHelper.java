package com.planetorora.utility;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


import com.planetorora.model.POPost;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by berjde on 2016. 1. 12..
 */
public class PostDBHelper extends SQLiteOpenHelper {

    private final static String ID_FIELD = "_id";
    private final static String USERID_FIELD = "_user_id";
    private final static String CONTENT_FIELD = "_content";
    private final static String IMAGE_FIELD = "_image";
    private final static String CREATED_FIELD = "_created";
    private final static String TABLE_NAME = "posts";
    private static PostDBHelper obj = null;
    private static final String[] COLUMNS = {ID_FIELD,USERID_FIELD, CONTENT_FIELD,IMAGE_FIELD, CREATED_FIELD};

    public static PostDBHelper getInstance(Context context){
        if(obj == null)
            obj = new PostDBHelper(context);

        return obj;
    }
    private PostDBHelper(Context context){
        super(context, "planetorara.db", null, 2);
    }
    public PostDBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table posts (_id integer primary key autoincrement,_user_id integer not null, _content text, _image text, _created integer )");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS posts");
        this.onCreate(db);
    }

    public void save(long userId, String content, String image){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(USERID_FIELD,userId);
        values.put(CONTENT_FIELD, content);
        values.put(IMAGE_FIELD, image);
        values.put(CREATED_FIELD, System.currentTimeMillis());


        db.insert(TABLE_NAME, null, values);
        db.close();
    }

    public List<POPost> getRecentPosts(long userId) {
        List<POPost> posts = new LinkedList<POPost>();

        String query = "SELECT  * FROM " + TABLE_NAME  + " WHERE _user_id = "+userId+" ORDER BY "+ID_FIELD+" desc limit 12";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        POPost post = null;

        if (cursor.moveToFirst()) {
            do {
                post = new POPost();
                post.id =Long.parseLong(cursor.getString(0));
                post.userId =Long.parseLong(cursor.getString(1));
                post.content = cursor.getString(2);
                post.image = cursor.getString(3);
                post.created = Long.parseLong(cursor.getString(4));
                posts.add(post);
                Log.d("post", post.toJson().toString());
            } while (cursor.moveToNext());
        }

        return posts;
    }

    public POPost getId(long mPostId) {


        String query = "SELECT  * FROM " + TABLE_NAME  + " WHERE _id = "+mPostId;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        POPost post = null;

        if (cursor.moveToFirst()) {
                post = new POPost();
                post.id =Long.parseLong(cursor.getString(0));
                post.userId =Long.parseLong(cursor.getString(1));
                post.content = cursor.getString(2);
                post.image = cursor.getString(3);
                post.created = Long.parseLong(cursor.getString(4));
                Log.d("post", post.toJson().toString());
        }

        return post;
    }

    public List<POPost> getList(long userId, int page, int size) {
        List<POPost> posts = new LinkedList<POPost>();

        String query = "SELECT  * FROM " + TABLE_NAME  + " WHERE _user_id = "+userId+" ORDER BY "+ID_FIELD+" desc limit "+0+","+ size;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        POPost post = null;

        if (cursor.moveToFirst()) {
            do {
                post = new POPost();
                post.id =Long.parseLong(cursor.getString(0));
                post.userId =Long.parseLong(cursor.getString(1));
                post.content = cursor.getString(2);
                post.image = cursor.getString(3);
                post.created = Long.parseLong(cursor.getString(4));
                posts.add(post);
                Log.d("post", post.toJson().toString());
            } while (cursor.moveToNext());
        }

        return posts;
    }
}
