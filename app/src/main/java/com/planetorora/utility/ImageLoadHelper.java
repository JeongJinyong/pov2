package com.planetorora.utility;

import android.content.Context;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

/**
 * Created by berjde on 2016. 1. 13..
 */
public class ImageLoadHelper {
    private static ImageLoader imageLoad;

    public static ImageLoader getInstance(Context context){
        imageLoad = ImageLoader.getInstance();
        if(!imageLoad.isInited()){


            DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .build();

            ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                    .defaultDisplayImageOptions(defaultOptions)
                    .build();

            imageLoad.init(config);
        }

        return imageLoad;
    }
}
