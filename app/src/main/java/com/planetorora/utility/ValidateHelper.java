package com.planetorora.utility;


import com.planetorora.model.POException;

/**
 * Created by berjde on 2016. 1. 3..
 */
public class ValidateHelper {
    private final static int PASSWORD_LENGTH = 4;
    private ValidateHelper(){}

    private static ValidateHelper obj = new ValidateHelper();

    public static ValidateHelper getInstance(){
        return obj;
    }

    public void signin(String email, String password) throws POException {
        if(!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches())
            throw new POException(400, "Invalids email address");

        if(password.length() < PASSWORD_LENGTH)
            throw new POException(400, "Password must be 4 characters");
    }

}
