package com.planetorora.newsfeed;

import android.os.Bundle;

import com.planetorora.R;
import com.planetorora.base.BaseActivity;
import com.planetorora.post.PostFragment;

public class NewsFeedActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_frame);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment, FeedFragment.newInstance())
                    .commit();
        }

        setToolbar("뉴스피드",null);
    }

}
