package com.planetorora.model;

/**
 * Created by berjde on 2016. 1. 3..
 */
public class POException extends Exception {
    public int errorCode;
    public String errorMessage;
    private POException() {}

    public POException(int errorCode, String message){
        super("errorCode : "+ errorCode + ", message : "+ message);
        this.errorCode = errorCode;
        this.errorMessage = message;

    }
}
