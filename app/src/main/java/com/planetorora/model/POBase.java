package com.planetorora.model;

import com.google.gson.Gson;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by berjde on 2016. 1. 2..
 */
public class POBase {
    private static Gson gson = new Gson();
    public String toJson(){
        return gson.toJson(this);
    }

    public static POPost init(String jsonString){
        POPost post = null;
        try {
            JSONObject json = new JSONObject(jsonString);
            post = new POPost();
            post.id = json.getLong("id");
            post.userId = json.getLong("userId");
            post.content = json.getString("content");
            post.image = json.isNull("image") ? null :json.getString("image");
            post.created = json.getLong("created");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return post;
    }
    public RequestParams toParams(){
        return null;
    }
}
