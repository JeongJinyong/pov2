package com.planetorora.model;

/**
 * Created by berjde on 2016. 1. 4..
 */
public class POPost extends  POBase {


    public enum POPostType{
        TEXT, IMAGE
    }

    public long id;
    public long userId;
    public String content;
    public POPostType type;
    public String image;
    public long created;
}
