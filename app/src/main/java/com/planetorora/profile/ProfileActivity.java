package com.planetorora.profile;

import android.app.Dialog;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.planetorora.R;
import com.planetorora.base.BaseActivity;

import org.json.JSONException;

/**
 * Created by berjde on 16. 3. 11..
 */
public class ProfileActivity extends BaseActivity {
    private Dialog mBottomSheetDialog;
    private View mBtnCamera, mBtnGallery, mBtnCancel,mDialogView;
    private TextView mNameView;
    private String mName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        try {
            mName =mAccount.getJSONObject("data").getString("name");
            setToolbar(mName, new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }


        setUI();


    }

    private void setUI() {
        mDialogView = getLayoutInflater ().inflate (R.layout.profile_edit_menu, null);
        mBottomSheetDialog = new Dialog (ProfileActivity.this,
                R.style.MaterialDialogSheet);

        mBottomSheetDialog.setContentView(mDialogView);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);

        View btnEdit = findViewById(R.id.profile_btn_edit);

        mBtnCamera = mDialogView.findViewById(R.id.profile_btn_camera);
        mBtnGallery = mDialogView.findViewById(R.id.profile_btn_gallery);
        mBtnCancel = mDialogView.findViewById(R.id.profile_btn_cancel);
        mNameView = (TextView)findViewById(R.id.profile_name);

        mNameView.setText(mName);

        mBtnCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        mBtnGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        mBtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.dismiss();
            }
        });


        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openBottomSheet(v);

            }
        });


    }


    public void openBottomSheet (View v) {


        mBottomSheetDialog.show ();

    }

}
