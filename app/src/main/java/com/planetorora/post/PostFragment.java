package com.planetorora.post;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import com.planetorora.R;
import com.planetorora.photo.CameraPreviewFragment;
import com.planetorora.photo.GalleryFragment;

/**
 * A placeholder fragment containing a simple view.
 */
public class PostFragment extends Fragment {
    private View mRootView, btnCamera, btnGallery, btnText;
    private TextView postText;
    private PostActivity mParent;

    public static PostFragment newInstance() {
        PostFragment fragment = new PostFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mRootView = inflater.inflate(R.layout.fragment_post, container, false);
        mParent = ((PostActivity)getActivity());
        setUI();

        return mRootView;
    }

    @Override
    public void onResume(){
        super.onResume();
        mParent.getSupportActionBar().show();
    }

    private void setUI(){
        mParent.setToolbar("작성하기", null);
        setBtnPost();
    }

    private void setBtnPost() {

        btnCamera = mRootView.findViewById(R.id.post_btn_camera);
        btnGallery = mRootView.findViewById(R.id.post_btn_gallery);
        btnText = mRootView.findViewById(R.id.post_btn_text);

        postText = (TextView) mRootView.findViewById(R.id.post_text);

        btnCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .addToBackStack(null)
                        .replace(R.id.fragment, CameraPreviewFragment.newInstance())
                        .commit();
            }
        });

        btnGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .addToBackStack(null)
                        .replace(R.id.fragment, GalleryFragment.newInstance())
                        .commit();
            }
        });

        btnText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .addToBackStack(null)
                        .replace(R.id.fragment, PostTextFragment.newInstance())
                        .commit();
            }
        });
    }

}
