package com.planetorora.post;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.planetorora.R;
import com.planetorora.base.ListSetting;
import com.planetorora.utility.ImageHelper;
import com.planetorora.utility.ImageLoadHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * A placeholder fragment containing a simple view.
 */
public class FilterFragment extends Fragment {
    private View mRootView;
    public String imagePath;
    public PostActivity mParent;
    private RecyclerView mList;
    private FilterAdapter mAdapter;
    private int mSelected = 0;
    private ImageLoader loader;
    private FilterViewHolder mPreviewSelected;

    public static FilterFragment newInstance() {
        FilterFragment fragment = new FilterFragment();
        return fragment;
    }

    @Override
    public void onResume(){

        super.onResume();
        mParent = (PostActivity) getActivity();
        mParent.getSupportActionBar().show();

        mParent.setToolbarRightBtn(R.drawable.pass_btn, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImagePreviewFragment frag = ImagePreviewFragment.newInstance();
                frag.imagePath = imagePath;
                mParent.getSupportFragmentManager().beginTransaction().addToBackStack(null)
                        .replace(R.id.fragment, frag )
                        .commit();
            }
        });

        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_filter, container, false);
        loader = ImageLoadHelper.getInstance(getActivity());
        setUI();
        return mRootView;
    }

    private void setUI() {
        setList();
        setImage();

    }

    private void setImage() {
        ImageView imageView = (ImageView) mRootView.findViewById(R.id.filter_image);
        loader.displayImage("file://" + imagePath, imageView);
    }

    private void setList() {
        mList = (RecyclerView) mRootView.findViewById(R.id.filter_list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mList.setLayoutManager(layoutManager);

        mAdapter = new FilterAdapter(R.layout.item_filter, new ListSetting() {
            @Override
            public void setUI(JSONArray data, Object holder, int position){}
            @Override
            public void setUI(JSONObject data, Object holder, int position){
                FilterViewHolder viewHolder = (FilterViewHolder)holder;
                try {
                    viewHolder.position = position;
                    viewHolder.pressed = false;
                    viewHolder.filterName.setText(data.getString("name"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        mList.setAdapter(mAdapter);

        requestData();


    }

    public void requestData() {
        String jsonString = "[{\"id\": 0, \"name\" : \"original\" }," +
                "{\"id\": 1, \"name\" : \"instant\"}," +
                "{\"id\": 2, \"name\" : \"vivid\"}," +
                "{\"id\": 3, \"name\" : \"country\"}," +
                "{\"id\": 4, \"name\" : \"overcast\"}," +
                "{\"id\": 5, \"name\" : \"ansel\"}," +
                "{\"id\": 6, \"name\" : \"holga\"}]";

        try {
            mAdapter.mData = new JSONArray(jsonString);
            mAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public class FilterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public Button filterName;
        public boolean pressed = false;
        public int position;
        public FilterViewHolder(View itemView) {
            super(itemView);
            filterName= (Button) itemView.findViewById(R.id.item_filter_name);
            filterName.setOnClickListener(this);

        }


        @Override
        public void onClick(View v) {
            Log.d("test", "pressed " + position);
            if(mPreviewSelected!=null) {
                mPreviewSelected.filterName.setBackgroundResource(R.drawable.btn_filter_normal);
                mPreviewSelected.pressed = false;
            }

            if(pressed) {
                pressed = false;
                v.setBackgroundResource(R.drawable.btn_filter_normal);
                mPreviewSelected = null;
                mSelected = -1;
            }
            else {
                pressed = true;
                v.setBackgroundResource(R.drawable.btn_filter_pressed);
                mPreviewSelected = this;
                mSelected = position;
            }

        }
    }

    public class FilterAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
        private int mResource;
        public JSONArray mData;
        private ListSetting mSetting;
        public FilterAdapter(){}
        public FilterAdapter(int resource, ListSetting setting){
            this.mResource = resource;
            this.mSetting = setting;
        }
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(mResource, parent, false);
            return new FilterViewHolder(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            try {
                mSetting.setUI(mData.getJSONObject(position), holder, position);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            if(mData == null) return 0;

            return mData.length();
        }


    }

}
