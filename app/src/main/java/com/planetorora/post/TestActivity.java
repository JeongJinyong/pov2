package com.planetorora.post;

import android.database.DataSetObserver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.planetorora.R;
import com.planetorora.utility.ImageLoadHelper;

public class TestActivity extends AppCompatActivity {
    private HorizontalScrollView scroll,scroll2, scroll3;
    private ImageView object;
    private Thread t, t2,t3,t4;
    //private Bitmap bm, bm2;
    private Spinner spinner;
    private boolean isFinish = true;
    private EditText speedView;
    private int startPoint;
    private int interval= 100;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        setGradation();
        setImage();

        speedView = (EditText) findViewById(R.id.test_speed);
        Button runButton= (Button) findViewById(R.id.test_run);
        runButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    interval = Integer.parseInt(speedView.getText().toString());
                }
                catch(Exception ex){
                    Toast.makeText(TestActivity.this, "you must input integer", Toast.LENGTH_LONG).show();
                }

            }
        });

        scroll = (HorizontalScrollView) findViewById(R.id.test_scroll);
        //startPoint = bm2.getWidth()- 100;
        scroll.scrollTo(startPoint,0);
        scroll2 = (HorizontalScrollView) findViewById(R.id.test_scroll2);
        scroll3 = (HorizontalScrollView) findViewById(R.id.test_scroll3);
        //object = (ImageView) findViewById(R.id.test_object);
        int height = getResources().getDisplayMetrics().heightPixels;
        //object.setX(0);
        //object.setY(height);
        scroll.setHorizontalScrollBarEnabled(false);
        scroll2.setHorizontalScrollBarEnabled(false);
        scroll3.setHorizontalScrollBarEnabled(false);



        scroll.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });

        scroll2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });

        scroll3.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });


        t = new Thread(new Runnable() {
            @Override
            public void run() {
                int i = 0;
                while(isFinish){
                    startPoint++;
                    scroll.scrollTo(startPoint,0);
                    try {
                        Thread.sleep(interval);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                int i = 0;
                while(isFinish){
                    i=i+1;
                    scroll2.scrollTo(i,0);
                    try {
                        Thread.sleep((long) (interval*0.9));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        t3 = new Thread(new Runnable() {
            @Override
            public void run() {
                int i = 0;
                while(isFinish){
                    i=i+1;
                    scroll3.scrollTo(i,0);
                    try {
                        Thread.sleep((long) (interval*0.8));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });


        t.start();
        t2.start();
        t3.start();

    }

    public void onDestroy(){
        super.onDestroy();
        isFinish = false;
//        bm2.recycle();
//        bm.recycle();
//        bm = null;
//        bm2 = null;
    }
    private void setGradation() {
        View view = findViewById(R.id.test_bg);
        View view2 = findViewById(R.id.test_bg2);
        view.setBackgroundResource(R.drawable.gradation_morning);
        view2.setBackgroundResource(R.drawable.gradation_sunset);
    }

    private void setImage() {
        ImageLoader loader= ImageLoadHelper.getInstance(TestActivity.this);

        ImageView mountainView = (ImageView) findViewById(R.id.test_mountain);
        ImageView mountainView2 = (ImageView) findViewById(R.id.test_mountain2);

        loader.displayImage("drawable://"+ R.drawable.mountain_morning, mountainView);
        loader.displayImage("drawable://"+ R.drawable.mountain_sunset, mountainView2);
        Log.d("test", mountainView.getWidth() + " , " + mountainView.getHeight());
    }

    public Bitmap getMountain() {
        long current = System.currentTimeMillis();
        return null;
    }

}
