package com.planetorora.post;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.WindowManager;

import com.planetorora.R;
import com.planetorora.base.BaseActivity;
import com.planetorora.emotion.EmotionActivityFragment;
import com.planetorora.friend.FriendListFragment;

public class PostActivity extends BaseActivity {

    @Override
    protected void onResume(){
        super.onResume();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_frame);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment, PostFragment.newInstance())
                    .commit();
        }

        setToolbar("작성하기", null);


    }

}
