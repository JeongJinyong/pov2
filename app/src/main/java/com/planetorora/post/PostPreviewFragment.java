package com.planetorora.post;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.planetorora.R;

/**
 * A placeholder fragment containing a simple view.
 */
public class PostPreviewFragment extends BasePreviewFragment {
    private View mRootView;
    public String content;


    public static PostPreviewFragment newInstance() {
        PostPreviewFragment fragment = new PostPreviewFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_post_preview, container, false);

        setUI();

        return mRootView;
    }

    private void setUI(){

        setDone();
        setPreviewSetting(mRootView);
    }

    private void setDone() {
        ((PostActivity)getActivity()).setToolbarRightBtn(R.drawable.check_btn, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });
    }
}
