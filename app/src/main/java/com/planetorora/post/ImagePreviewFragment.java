package com.planetorora.post;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.planetorora.R;
import com.planetorora.utility.ImageHelper;
import com.planetorora.utility.ImageLoadHelper;

/**
 * A placeholder fragment containing a simple view.
 */
public class ImagePreviewFragment extends BasePreviewFragment {
    private View mRootView;
    public String imagePath;
    public ImageLoader loader;
    public PostActivity mParent;

    public static ImagePreviewFragment newInstance() {
        ImagePreviewFragment fragment = new ImagePreviewFragment();
        return fragment;
    }

    @Override
    public void onResume(){
        super.onResume();
        mParent = (PostActivity) getActivity();
        mParent.getSupportActionBar().show();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_image_preview, container, false);
        loader = ImageLoadHelper.getInstance(getActivity());
        setUI();
        return mRootView;
    }

    private void setUI(){
        setLoadImage();
        setDone();
        setPreviewSetting(mRootView);
    }

    private void setLoadImage() {

        ImageView view = (ImageView) mRootView.findViewById(R.id.post_preview_image);
        loader.displayImage("file://"+imagePath , view);
    }

    private void setDone() {
        ((PostActivity)getActivity()).setToolbarRightBtn(R.drawable.check_btn, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });
    }

    
}
