package com.planetorora.post;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.planetorora.R;

/**
 * A placeholder fragment containing a simple view.
 */
public class PostTextFragment extends Fragment {
    private View mRootView, btnCamera, btnGallery, btnText;
    private EditText postText;

    public static PostTextFragment newInstance() {
        PostTextFragment fragment = new PostTextFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_post_text, container, false);

        setUI();

        return mRootView;
    }

    private void setUI(){
        showKeyboard();
        showNextButton();

    }

    private void showNextButton() {
        ((PostActivity)getActivity()).setToolbarRightBtn(R.drawable.pass_btn, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PostPreviewFragment frag = PostPreviewFragment.newInstance();
                frag.content = postText.getText().toString();
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .addToBackStack(null)
                        .replace(R.id.fragment, frag,"TEXT_PREVIEW")
                        .commit();
            }
        });
    }

    private void showKeyboard() {
        postText = (EditText) mRootView.findViewById(R.id.post_text);
        postText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) return;

                postText.post(new Runnable() {
                    @Override
                    public void run() {
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.showSoftInput(postText, InputMethodManager.SHOW_IMPLICIT);
                    }
                });
            }
        });
        postText.requestFocus();

    }

    @Override
    public void onPause() {
        super.onPause();
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(postText.getWindowToken(), 0);
    }
}
