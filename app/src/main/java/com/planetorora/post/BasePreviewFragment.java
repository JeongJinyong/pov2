package com.planetorora.post;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.planetorora.R;
import com.planetorora.base.AppConstants;
import com.planetorora.emotion.EmotionActivity;
import com.planetorora.emotion.EmotionRecordActivity;

/**
 * Created by berjde on 16. 3. 13..
 */
public class BasePreviewFragment extends Fragment {
    public View mParentView;

    @Override
    public void onResume(){
        super.onResume();
        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    public void setPreviewSetting(View parentView){
        mParentView = parentView;

        SwitchCompat btnPrivate = (SwitchCompat) parentView.findViewById(R.id.preview_btn_private);

        btnPrivate.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                togglePrivateBtn(isChecked);
            }
        });

        View emotionView = parentView.findViewById(R.id.preview_emotion_box);
        emotionView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(getActivity(), EmotionRecordActivity.class);

                startActivityForResult(i, AppConstants.INTENT_EMOTION);

            }
        });

        View tagView = parentView.findViewById(R.id.preview_tag_box);
        tagView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), TagActivity.class);

                startActivityForResult(i, AppConstants.INTENT_TAG);

            }
        });

    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode){
            case AppConstants.INTENT_EMOTION :
                if(resultCode == getActivity().RESULT_OK){
                    if( data.getExtras().get("data") != null){

                    }
                    else{

                    }
                }
                break;
            case AppConstants.INTENT_TAG :
                if(resultCode == getActivity().RESULT_OK){
                    if( data.getExtras().get("data") != null){

                    }
                    else{

                    }
                }
                break;
        }
    }


    private void togglePrivateBtn(boolean isChecked){
        TextView privateText = (TextView) mParentView.findViewById(R.id.preview_private_text);
        if(isChecked){
            privateText.setTextColor(getResources().getColor(R.color.colorPrimary));
            privateText.setCompoundDrawablesWithIntrinsicBounds( R.drawable.icon_key_s, 0, 0, 0);

        }
        else{
            privateText.setTextColor(getResources().getColor(R.color.disabled_text));
            privateText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_key_n, 0, 0, 0);
        }

    }

    private void toggleEmotionView(boolean hasData){
        TextView emotionText = (TextView) mParentView.findViewById(R.id.preview_emotion_text);

    }


    private void toggleTagView(boolean hasData){
        TextView tagText = (TextView) mParentView.findViewById(R.id.preview_tag_text);

    }
}

