package com.planetorora;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.planetorora.planet.PlanetActivity;
import com.planetorora.user.SignActivity;
import com.planetorora.utility.AccountHelper;

/**
 * Created by berjde on 16. 3. 10..
 */
public class SplashActivity extends AppCompatActivity {
    private static final String TAG = "SplashActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);

        Thread splash_thread = new Thread(
                new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(1000);

                        } catch (InterruptedException e) {
                        }

//                        if( AccountHelper.getUserInfo(getBaseContext()) == null) {
//                            Intent intent = new Intent(getBaseContext(), SigninActivity.class);
//                            startActivity(intent);
//                        }
//                        else{
//                            Intent intent = new Intent(getBaseContext(), PlanetActivity.class);
//                            startActivity(intent);
//                        }

                        Intent intent;
                        if(AccountHelper.getUserInfo(SplashActivity.this) != null){
                            intent = new Intent(getBaseContext(), PlanetActivity.class);
                        }
                        else{
                            intent = new Intent(getBaseContext(), SignActivity.class);
                        }

                        startActivity(intent);
                        finish();

                        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

                    }
                }
        );


        splash_thread.start();


    }
}
