package com.planetorora.emotion;

import android.os.Bundle;
import android.view.View;

import com.planetorora.R;
import com.planetorora.base.BaseActivity;

public class EmotionRecordActivity extends BaseActivity {
    private EmotionRecordFragment mFrag;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_frame);
        if (savedInstanceState == null) {
            mFrag = EmotionRecordFragment.newInstance();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment, mFrag)
                    .commit();
        }

        setToolbar("감성기록",null);

    }

}
