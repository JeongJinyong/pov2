package com.planetorora.emotion;

import android.os.Bundle;

import com.planetorora.R;
import com.planetorora.base.AppConstants;
import com.planetorora.base.BaseActivity;

public class EmotionActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_frame);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment, EmotionActivityFragment.newInstance())
                    .commit();
        }

        setToolbar("감성기록", null);

    }

}
