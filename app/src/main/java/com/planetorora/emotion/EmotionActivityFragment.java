package com.planetorora.emotion;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.planetorora.R;

/**
 * A placeholder fragment containing a simple view.
 */
public class EmotionActivityFragment extends Fragment {
    private View mRootView;
    public int mSelected = -1;
    public static EmotionActivityFragment newInstance() {
        EmotionActivityFragment fragment = new EmotionActivityFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_emotion, container, false);

        setUI();

        return mRootView;
    }

    private void setUI() {
        setBtnRecord();
    }

    private void setBtnRecord() {
        View btnRecord = mRootView.findViewById(R.id.emotion_btn_record);
        btnRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .addToBackStack(null)
                        .replace(R.id.fragment, EmotionRecordFragment.newInstance())
                        .commit();

                ((EmotionActivity)getActivity()).setToolbar("감성기록", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });
            }
        });
    }



}
