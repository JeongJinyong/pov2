package com.planetorora.emotion;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.planetorora.R;
import com.planetorora.base.ListSetting;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * A placeholder fragment containing a simple view.
 */
public class EmotionRecordFragment extends Fragment {
    private final static String LOG = "EmotionRecordFragment";
    private View mRootView;
    private RecyclerView mListView;
    private GridLayoutManager mLayoutManager;
    private EmotionAdapter mAdapter;
    public int selected = -1;
    private EmotionViewHolder prevSelected;

    public static EmotionRecordFragment newInstance() {
        EmotionRecordFragment fragment = new EmotionRecordFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_list, container, false);

        setUI();
        return mRootView;
    }

    private void setUI(){
        setListView();
    }

    private void setListView()  {

        mListView = (RecyclerView) mRootView.findViewById(R.id.recycler_view);
        int i = getResources().getDisplayMetrics().densityDpi / 160;
        mListView.setPadding(i*12,0,i*12,0);

        mLayoutManager = new GridLayoutManager(getActivity(), 3);
        mListView.setLayoutManager(mLayoutManager);


        mAdapter = new EmotionAdapter(R.layout.item_emotion_grid, EmotionViewHolder.class, new ListSetting() {
            @Override
            public void setUI(JSONArray data, Object holder, int position) {}
            @Override
            public void setUI(JSONObject data, Object holder, int position) {
                EmotionViewHolder eventViewHolder = (EmotionViewHolder)holder;
                eventViewHolder.position = position;
                eventViewHolder.pressed = false;
                switch(position){
                    case 0 :
                        eventViewHolder.name.setText("Sunny");
                        eventViewHolder.image.setImageResource(R.drawable.feel_sunny);
                        break;
                    case 1:
                        eventViewHolder.name.setText("Cloudy");
                        eventViewHolder.image.setImageResource(R.drawable.feel_cloudy);
                        break;

                    case 2:
                        eventViewHolder.name.setText("Rainy");
                        eventViewHolder.image.setImageResource(R.drawable.feel_rainy);
                        break;

                    case 3:
                        eventViewHolder.name.setText("Frigid");
                        eventViewHolder.image.setImageResource(R.drawable.feel_frigid);
                        break;

                    case 4:
                        eventViewHolder.name.setText("Stormy");
                        eventViewHolder.image.setImageResource(R.drawable.feel_stormy);
                        break;

                    case 5:
                        eventViewHolder.name.setText("Foggy");
                        eventViewHolder.image.setImageResource(R.drawable.feel_foggy);
                        break;

                    case 6:
                        eventViewHolder.name.setText("Cleared up");
                        eventViewHolder.image.setImageResource(R.drawable.feel_cleared);
                        break;

                    case 7:
                        eventViewHolder.name.setText("Unsettled");
                        eventViewHolder.image.setImageResource(R.drawable.feel_unsettled);
                        break;

                    case 8:
                        eventViewHolder.name.setText("Windy");
                        eventViewHolder.image.setImageResource(R.drawable.feel_windy);
                        break;
                }
            }
        });


        mListView.setAdapter(mAdapter);


        try {
            mAdapter.mData = new JSONArray("[{},{},{},{},{},{},{},{},{}]");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        mAdapter.notifyDataSetChanged();


    }
    public class EmotionViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public int position;
        public ImageButton image;
        public TextView name;
        public boolean pressed = false;

        public EmotionViewHolder(View itemView) {
            super(itemView);
            image = (ImageButton) itemView.findViewById(R.id.emotion_image);
            name= (TextView) itemView.findViewById(R.id.emotion_text);

            image.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(prevSelected!=null) {
                prevSelected.image.setBackgroundResource(R.drawable.emotion_btn_bg);
                prevSelected.pressed = false;
            }

            if(pressed) {
                pressed = false;
                image.setBackgroundResource(R.drawable.emotion_btn_bg);
                prevSelected = null;
                selected = -1;
            }
            else {
                pressed = true;
                image.setBackgroundResource(R.drawable.emotion_btn_pressed_bg);
                prevSelected = this;
                selected = position;
            }



        }
    }

    public class EmotionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        public JSONArray mData;
        private ListSetting mSetting;
        private int mResource;
        private Class<?> clazz;
        private EmotionAdapter(){}

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(mResource, parent, false);

            return new EmotionViewHolder(view);

        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            try {
                mSetting.setUI(mData.getJSONObject(position), holder, position);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        @Override
        public int getItemCount() {
            return mData.length();
        }

        public EmotionAdapter(int resource, Class<?> clazz, ListSetting setting){
            this.mData = new JSONArray();
            this.mSetting = setting;
            this.mResource = resource;
            this.clazz = clazz;

        }



    }

}
