package com.planetorora.user;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.planetorora.R;

/**
 * A placeholder fragment containing a simple view.
 */
public class SignDescFragment extends Fragment {
    private View mRootView;
    public static SignDescFragment newInstance() {
        SignDescFragment fragment = new SignDescFragment();
        return fragment;
    }

    @Override
    public void onResume(){

        super.onResume();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_sign_desc, container, false);

        return mRootView;
    }
}
