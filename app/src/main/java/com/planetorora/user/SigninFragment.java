package com.planetorora.user;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.planetorora.R;
import com.planetorora.model.POException;
import com.planetorora.planet.PlanetActivity;
import com.planetorora.utility.AccountHelper;
import com.planetorora.utility.AsyncHttpHelper;
import com.planetorora.utility.ValidateHelper;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

/**
 * A placeholder fragment containing a simple view.
 */
public class SigninFragment extends Fragment  implements TextWatcher{
    private View mRootView, btnSignin;
    private TextView validationMessage;
    private EditText editEmail, editPassword;
    private boolean isValid = false;
    private ProgressDialog mDialog;
    public static SigninFragment newInstance() {
        SigninFragment fragment = new SigninFragment();
        return fragment;
    }

    @Override
    public void onResume(){

        super.onResume();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_signin, container, false);
        setUI();
        return mRootView;
    }

    private void setUI() {
        setWatcher();
        setSignBtn();
    }

    private void setWatcher() {
        editEmail = (EditText) mRootView.findViewById(R.id.signin_email);
        editPassword = (EditText) mRootView.findViewById(R.id.signin_password);
        validationMessage = (TextView) mRootView.findViewById(R.id.signin_validation_message);
        editEmail.addTextChangedListener(this);
        editPassword.addTextChangedListener(this);
    }

    private void setSignBtn() {
        btnSignin = mRootView.findViewById(R.id.signin_btn);
        btnSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValid)
                    postSession();
                else {
                }
            }
        });
    }


    private void postSession(){
        RequestParams params = new RequestParams();
        final String email = editEmail.getText().toString();
        final String password = editPassword.getText().toString();

        params.add("post", String.format("{\"email\" : \"%s\", \"password\":\"%s\"}", email, password ));

        mDialog = ProgressDialog.show(getActivity(), null, getString(R.string.loading_text));

        AsyncHttpHelper.post("session", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.d("test", statusCode + ", " + response.toString());
                mDialog.dismiss();
                try {
                    JSONObject result = response.getJSONObject("error");

                    if (result.getInt("code") == 0) {//success to get a result.
                        AccountHelper.save(getActivity(), email, password, response.getJSONObject("res"));
                        Intent i = new Intent(getActivity(), PlanetActivity.class);
                        startActivity(i);
                        getActivity().finish();
                        getActivity().overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.d("test", statusCode + ", " + responseString);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response) {
                Log.d("test", statusCode + ", " + response.toString());
                mDialog.dismiss();
                try {
                    JSONObject errorObj = response.getJSONObject("error");
                    validationMessage.setText(errorObj.getInt("code") + "," + errorObj.getString("msg"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        });

    }

    // implements TextWatcher
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        try {
            ValidateHelper.getInstance().signin(editEmail.getText().toString(), editPassword.getText().toString());
            isValid = true;
            validationMessage.setText("");
        } catch (POException e) {
            validationMessage.setText(e.errorMessage);
        }
    }
}
