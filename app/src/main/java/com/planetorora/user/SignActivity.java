package com.planetorora.user;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.planetorora.R;
import com.planetorora.base.BaseActivity;

public class SignActivity extends AppCompatActivity {

    private enum PAGE {
        DESC, SIGNUP, SIGNIN
    }
    private Button btnSignin, btnSignup;
    private PAGE mSelected = PAGE.DESC;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_sign);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment, SignDescFragment.newInstance())
                    .commit();
        }

        setUI();
    }

    private void setUI(){
        btnSignin = (Button) findViewById(R.id.sign_btn_signin);
        btnSignup = (Button) findViewById(R.id.sign_btn_signup);


        btnSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                changePage(PAGE.SIGNIN);
            }
        });

        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                changePage(PAGE.SIGNUP);
            }
        });


    }
    private void changePage(PAGE page){

        switch(page){
            case DESC:
                getSupportFragmentManager().beginTransaction()
                        .addToBackStack(null)
                        .replace(R.id.fragment, SignDescFragment.newInstance())
                        .commit();
                break;

            case SIGNIN:
                getSupportFragmentManager().beginTransaction()
                        .addToBackStack(null)
                        .replace(R.id.fragment, SigninFragment.newInstance())
                        .commit();
                break;

            case SIGNUP:
                getSupportFragmentManager().beginTransaction()
                        .addToBackStack(null)
                        .replace(R.id.fragment, SignupFragment.newInstance())
                        .commit();
                break;
        }

    }

}
