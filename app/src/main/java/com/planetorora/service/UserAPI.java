package com.planetorora.service;

import android.util.Log;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.planetorora.utility.AsyncHttpHelper;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

/**
 * Created by berjde on 16. 5. 9..
 */
public class UserAPI {
    public void getUser(String email, String password, final UIAction action){
        RequestParams params = new RequestParams();

        params.add("post", String.format("{\"email\" : \"%s\", \"password\":\"%s\"}", email, password));


        AsyncHttpHelper.post("session", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.d("test", statusCode + ", " + response.toString());

                try {
                    JSONObject result = response.getJSONObject("error");

                    if (result.getInt("code") == 0) {//success to get a result.
                        action.update(true, response.getJSONObject("res"));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.d("test", statusCode + ", " + responseString);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response) {
                Log.d("test", statusCode + ", " + response.toString());
                try {
                    JSONObject errorObj = response.getJSONObject("error");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        });

    }


    public void getUserInfo(String userId, String key, final UIAction action){

        AsyncHttpHelper.get(userId, key, "user", null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.d("test", statusCode + ", " + response.toString());

                try {
                    JSONObject result = response.getJSONObject("error");

                    if (result.getInt("code") == 0) {//success to get a result.
                        action.update(true, response.getJSONObject("res"));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.d("test", statusCode + ", " + responseString);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response) {
                Log.d("test", statusCode + ", " + response.toString());
                try {
                    JSONObject errorObj = response.getJSONObject("error");

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        });
    }
}
