package com.planetorora.service;

import org.json.JSONObject;

/**
 * Created by berjde on 16. 5. 9..
 */
public interface UIAction {
    public void update(boolean success, JSONObject obj);
}
