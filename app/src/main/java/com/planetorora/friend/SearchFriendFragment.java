package com.planetorora.friend;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.planetorora.R;
import com.planetorora.utility.AccountHelper;
import com.planetorora.utility.AsyncHttpHelper;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

/**
 * A placeholder fragment containing a simple view.
 */
public class SearchFriendFragment extends Fragment {
    private SearchView mSearchView;
    private View mRootView;
    private RecyclerView mListView;
    private LinearLayoutManager mLayoutManager;

    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    private FriendManagerActivity mParent;
    public static SearchFriendFragment newInstance() {

        SearchFriendFragment fragment = new SearchFriendFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mParent = (FriendManagerActivity) getActivity();
        mRootView = inflater.inflate(R.layout.fragment_list, container, false);

        setUI();

        return mRootView;
    }

    private void setUI(){
        setListView();

    }

    private void setListView()  {
        mSearchView = (SearchView) mRootView.findViewById(R.id.recycler_search_view);
        mSearchView.setVisibility(View.VISIBLE);
        mSearchView.setQueryHint(getString(R.string.search_friend_hint));

        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                // TODO Auto-generated method stub
                getRecommentList(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                // TODO Auto-generated method stub

                return false;
            }
        });

        mListView = (RecyclerView) mRootView.findViewById(R.id.recycler_view);
        int i = getResources().getDisplayMetrics().densityDpi / 160;
        mListView.setPadding(i*20,0,i*20,0);

        mLayoutManager = new LinearLayoutManager(getActivity());
        mListView.setLayoutManager(mLayoutManager);



        mListView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();
                    Log.d("test", totalItemCount + ", " + visibleItemCount + ", " + pastVisiblesItems);
                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            Log.d("test", "loading...");
                            loading = false;

                        }
                    }
                }
            }
        });

    }


    private void getRecommentList(String targetId){
        JSONObject userInfo = AccountHelper.getUserInfo(getActivity());
        String userId=null, userKey=null;
        Log.d("test", userInfo.toString());

        try {
            userId = userInfo.getString("id");
            userKey = userInfo.getString("key");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RequestParams params = new RequestParams();
        params.add("post", String.format("{\"user_to_id\" : %d}", Integer.parseInt(targetId)));


        AsyncHttpHelper.post(userId, userKey, "user/friend", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.d("test", statusCode + ", " + response.toString());

                try {
                    JSONObject result = response.getJSONObject("error");

                    if (result.getInt("code") == 0) {//success to get a result.

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.d("test", statusCode + ", " + responseString);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response) {
                Log.d("test", statusCode + ", " + response.toString());

                try {
                    JSONObject errorObj = response.getJSONObject("error");
                    Toast.makeText(getActivity(), errorObj.getString("msg"), Toast.LENGTH_LONG).show();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        });
    }

}
