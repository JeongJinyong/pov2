package com.planetorora.friend;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.planetorora.R;
import com.planetorora.base.FriendHeaderViewHolder;
import com.planetorora.base.FriendViewHolder;
import com.planetorora.base.ListSetting;
import com.planetorora.utility.AccountHelper;
import com.planetorora.utility.AsyncHttpHelper;
import com.planetorora.utility.ImageLoadHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

/**
 * A placeholder fragment containing a simple view.
 */
public class FriendManagerFragment extends Fragment {
    private ImageLoader loader;
    private View mRootView;
    private ProgressDialog mDialog;
    private RecyclerView mListView;
    private LinearLayoutManager mLayoutManager;
    private FriendListAdapter mAdapter;
    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;

    public static FriendManagerFragment newInstance() {
        FriendManagerFragment fragment = new FriendManagerFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_list, container, false);

        setUI();

        return mRootView;
    }

    private void setUI(){
        loader = ImageLoadHelper.getInstance(getActivity());
        setListView();
        getFriendList();
    }

    private void setListView()  {

        mListView = (RecyclerView) mRootView.findViewById(R.id.recycler_view);

        mLayoutManager = new LinearLayoutManager (getActivity());
        mListView.setLayoutManager(mLayoutManager);

        mAdapter = new FriendListAdapter(R.layout.item_friend_type_1, new ListSetting() {
            @Override
            public void setUI(JSONObject data, Object holder, int position){}

            @Override
            public void setUI(JSONArray data, Object holder, int position) {


                try {
                    if(data.get(0) instanceof  JSONObject){
                        FriendHeaderViewHolder viewHolder = (FriendHeaderViewHolder)holder;
                        if(data.getJSONObject(0).getBoolean("isRecvHeader")){

                            viewHolder.title.setText("친구가 나에게 요청");
                        }
                        else{
                            viewHolder.title.setText("내가 친구에게 요청");
                        }
                    }
                    else{
                        FriendViewHolder viewHolder = (FriendViewHolder)holder;
                        final int userId = data.getInt(0);
                        String userName = data.getString(1);
                        String imageUrl = data.getString(2);

                        viewHolder.userId = userId;
                        viewHolder.name.setText(userName);
                        if(imageUrl.equals("")){
                            loader.displayImage("drawable://"+R.drawable.profile_icon1, viewHolder.image);
                        }
                        if(data.getJSONObject(data.length()-1).getBoolean("isRecv")){
                            viewHolder.action.setText("수락");
                            viewHolder.refuse.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                }
                            });

                            viewHolder.action.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    accept(String.valueOf(userId));
                                    ((Button)v).setText("수락됨");
                                }
                            });
                        }
                        else{
                            viewHolder.action.setVisibility(View.GONE);
                            viewHolder.refuse.setText("취소");
                        }
                        //boolean isBlock = !data.isNull("isBlock") && data.getBoolean("isBlock") ? true : false;
                        //boolean isNew = !data.isNull("new") && data.getBoolean("new") ? true : false;

                        //viewHolder.mark.setVisibility(isNew ? View.VISIBLE :View.GONE);
                        //viewHolder.mask.setVisibility(isBlock ? View.VISIBLE :View.GONE);
                        //viewHolder.block.setVisibility(isBlock ? View.VISIBLE :View.GONE);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });


        mListView.setAdapter(mAdapter);

        mListView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();
                    Log.d("test", totalItemCount + ", " + visibleItemCount + ", " + pastVisiblesItems);
                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            Log.d("test", "loading...");
                            loading = false;

                        }
                    }
                }
            }
        });

    }


    private void getFriendList(){
        JSONObject userInfo = AccountHelper.getUserInfo(getActivity());
        String userId=null, userKey=null;
        Log.d("test", userInfo.toString());

        try {
            userId = userInfo.getString("id");
            userKey = userInfo.getString("key");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RequestParams params = new RequestParams();

        mDialog = ProgressDialog.show(getActivity(), null, getString(R.string.loading_text));

        AsyncHttpHelper.get(userId, userKey, "user/friend", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.d("test", statusCode + ", " + response.toString());
                mDialog.dismiss();
                try {
                    JSONObject result = response.getJSONObject("error");

                    if (result.getInt("code") == 0) {//success to get a result.
                        mAdapter.mData = new JSONArray();
                        JSONArray recvObj = response.getJSONObject("res").getJSONArray("recv");
                        JSONArray sendObj = response.getJSONObject("res").getJSONArray("send");

                        mAdapter.mData.put(new JSONArray().put(new JSONObject().put("isRecvHeader", false)));
                        for(int i=0;i < sendObj.length(); i++){
                            sendObj.getJSONArray(i).put(new JSONObject().put("isRecv", false));
                            mAdapter.mData.put(sendObj.getJSONArray(i));
                        }

                        mAdapter.mData.put(new JSONArray().put(new JSONObject().put("isRecvHeader", true)));
                        for(int i=0;i < recvObj.length(); i++){
                            recvObj.getJSONArray(i).put(new JSONObject().put("isRecv", true));
                            mAdapter.mData.put(recvObj.getJSONArray(i));
                        }

                        Log.d("test", mAdapter.mData.toString());
                        mAdapter.notifyDataSetChanged();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.d("test", statusCode + ", " + responseString);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response) {
                Log.d("test", statusCode + ", " + response.toString());
                mDialog.dismiss();
                try {
                    JSONObject errorObj = response.getJSONObject("error");

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        });
    }


    public class FriendListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        public JSONArray mData;
        private ListSetting mSetting;
        private int mResource;
        private FriendListAdapter(){}

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            Log.d("test", "list type : "+ viewType);
            View view;
            if(viewType >= 1){
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_friend_header, parent, false);
                return new FriendHeaderViewHolder(view);
            }
            else {
                view = LayoutInflater.from(parent.getContext()).inflate(mResource, parent, false);
                return new FriendViewHolder(view);
            }

        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            try {
                mSetting.setUI(mData.getJSONArray(position), holder, position);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        @Override
        public int getItemCount() {
            return mData.length();
        }

        public FriendListAdapter(int resource, ListSetting setting){
            this.mData = new JSONArray();
            this.mSetting = setting;
            this.mResource = resource;
        }

        @Override
        public int getItemViewType(int position) {
            int retVal = 0;
            try {
                JSONArray obj = mData.getJSONArray(position);
                if(obj.get(0) instanceof  JSONObject){
                    if(obj.getJSONObject(0).getBoolean("isRecvHeader")){
                        retVal = 1;
                    }
                    else{
                        retVal = 2;
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return retVal;

        }

    }


    private void accept(String targetId){
        JSONObject userInfo = AccountHelper.getUserInfo(getActivity());
        String userId=null, userKey=null;
        Log.d("test", userInfo.toString());

        try {
            userId = userInfo.getString("id");
            userKey = userInfo.getString("key");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RequestParams params = new RequestParams();
        params.add("post", String.format("{\"user_to_id\" : %d}", Integer.parseInt(targetId)));


        AsyncHttpHelper.post(userId, userKey, "user/friend", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.d("test", statusCode + ", " + response.toString());

                try {
                    JSONObject result = response.getJSONObject("error");

                    if (result.getInt("code") == 0) {//success to get a result.

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.d("test", statusCode + ", " + responseString);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response) {
                Log.d("test", statusCode + ", " + response.toString());

                try {
                    JSONObject errorObj = response.getJSONObject("error");
                    Toast.makeText(getActivity(), errorObj.getString("msg"), Toast.LENGTH_LONG).show();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        });
    }

}
