package com.planetorora.friend;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.planetorora.R;
import com.planetorora.base.FriendViewHolder;
import com.planetorora.base.ListSetting;
import com.planetorora.utility.AccountHelper;
import com.planetorora.utility.AsyncHttpHelper;
import com.planetorora.utility.ImageLoadHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

/**
 * A placeholder fragment containing a simple view.
 */
public class FriendListFragment extends Fragment {
    private ImageLoader loader;
    private View mRootView;
    private ProgressDialog mDialog;
    private RecyclerView mListView;
    private GridLayoutManager mLayoutManager;
    private FriendListAdapter mAdapter;
    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;

    public static FriendListFragment newInstance() {
        FriendListFragment fragment = new FriendListFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_list, container, false);

        setUI();

        return mRootView;
    }

    private void setUI(){
        loader = ImageLoadHelper.getInstance(getActivity());
        setListView();
        getFriendList();
    }

    private void setListView()  {

        mListView = (RecyclerView) mRootView.findViewById(R.id.recycler_view);
        int i = getResources().getDisplayMetrics().densityDpi / 160;
        mListView.setPadding(i*20,0,i*20,0);

        mLayoutManager = new GridLayoutManager(getActivity(), 3);
        mListView.setLayoutManager(mLayoutManager);

        mAdapter = new FriendListAdapter(R.layout.item_friend, new ListSetting() {
            @Override
            public void setUI(JSONObject data, Object holder, int position){}

            @Override
            public void setUI(JSONArray data, Object holder, int position) {
                FriendViewHolder viewHolder = (FriendViewHolder)holder;

                try {
                    int userId = data.getInt(0);
                    String userName = data.getString(1);
                    String imageUrl = data.getString(2);
                    viewHolder.userId = userId;
                    viewHolder.name.setText(userName);
                    if(imageUrl.equals("")){
                        loader.displayImage("drawable://"+R.drawable.profile_icon1, viewHolder.image);
                    }
                    //boolean isBlock = !data.isNull("isBlock") && data.getBoolean("isBlock") ? true : false;
                    //boolean isNew = !data.isNull("new") && data.getBoolean("new") ? true : false;

                    //viewHolder.mark.setVisibility(isNew ? View.VISIBLE :View.GONE);
                    //viewHolder.mask.setVisibility(isBlock ? View.VISIBLE :View.GONE);
                    //viewHolder.block.setVisibility(isBlock ? View.VISIBLE :View.GONE);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });


        mListView.setAdapter(mAdapter);

        mListView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();
                    Log.d("test", totalItemCount + ", " + visibleItemCount + ", " + pastVisiblesItems);
                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            Log.d("test", "loading...");
                            loading = false;

                        }
                    }
                }
            }
        });

    }


    private void getFriendList(){
        JSONObject userInfo = AccountHelper.getUserInfo(getActivity());
        String userId=null, userKey=null;
        Log.d("test", userInfo.toString());

        try {
            userId = userInfo.getString("id");
            userKey = userInfo.getString("key");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RequestParams params = new RequestParams();

        mDialog = ProgressDialog.show(getActivity(), null, getString(R.string.loading_text));

        AsyncHttpHelper.get(userId, userKey, "user/friend", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.d("test", statusCode + ", " + response.toString());
                mDialog.dismiss();
                try {
                    JSONObject result = response.getJSONObject("error");

                    if (result.getInt("code") == 0) {//success to get a result.
                        mAdapter.mData = response.getJSONObject("res").getJSONArray("mutual");
                        mAdapter.notifyDataSetChanged();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.d("test", statusCode + ", " + responseString);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response) {
                Log.d("test", statusCode + ", " + response.toString());
                mDialog.dismiss();
                try {
                    JSONObject errorObj = response.getJSONObject("error");

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        });
    }


    public class FriendListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        public JSONArray mData;
        private ListSetting mSetting;
        private int mResource;
        private FriendListAdapter(){}

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(mResource, parent, false);
            return new FriendViewHolder(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            try {
                mSetting.setUI(mData.getJSONArray(position), holder, position);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        @Override
        public int getItemCount() {
            return mData.length();
        }

        public FriendListAdapter(int resource, ListSetting setting){
            this.mData = new JSONArray();
            this.mSetting = setting;
            this.mResource = resource;
        }

        @Override
        public int getItemViewType(int position) {
            boolean isHeader = false;
//            try {
//
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }

            return isHeader == true? 1 : 0 ;

        }

    }

}
