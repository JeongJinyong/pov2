package com.planetorora.friend;

import android.os.Bundle;

import com.planetorora.R;
import com.planetorora.base.AppConstants;
import com.planetorora.base.BaseActivity;

public class FriendActivity extends BaseActivity {

    public static final String LIKED_LIST = "likedList";
    public static final String FRIEND_LIST = "friendList";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_frame);

        String viewType = getIntent().getExtras().getString(AppConstants.FRIEND_VIEW);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment, FriendListFragment.newInstance())
                    .commit();
        }

        setToolbar(viewType.equals(LIKED_LIST) ? "좋아요" : "친구", null);

    }

}
