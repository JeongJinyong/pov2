package com.planetorora.friend;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import com.planetorora.R;
import com.planetorora.base.AppConstants;
import com.planetorora.base.BaseActivity;
import com.planetorora.newsfeed.FeedFragment;
import com.planetorora.planet.BlankFragment;

public class FriendManagerActivity extends BaseActivity {
    public TabLayout tabLayout;
    public ViewPager viewPager;
    public TabPagerAdapter mTapAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tablayout);

        setTab();

        setToolbar("친구", null);

    }

    private void setTab() {
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        mTapAdapter = new TabPagerAdapter(getSupportFragmentManager());

        viewPager.setAdapter(mTapAdapter);

        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabsFromPagerAdapter(mTapAdapter);

        for(int i=0; i < tabLayout.getTabCount(); i++){
        }
    }

    private class TabPagerAdapter extends FragmentPagerAdapter {

        public TabPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        public String[] tabs = new String[]{"목록", "찾기", "추천", "관리"};

        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:
                    return FriendListFragment.newInstance();

                case 1:
                    return SearchFriendFragment.newInstance();

                case 2:
                    return BlankFragment.newInstance();

                case 3:
                    return FriendManagerFragment.newInstance();

                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return tabs.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {

            return tabs[position];
        }
    }

}
