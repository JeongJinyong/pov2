package com.planetorora.base;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.planetorora.R;

/**
 * Created by berjde on 2015. 12. 17..
 */
public class FriendHeaderViewHolder extends RecyclerView.ViewHolder {

    public TextView title;

    public FriendHeaderViewHolder(View itemView) {
        super(itemView);

        title = (TextView) itemView.findViewById(R.id.item_friend_header_name);

    }

}
