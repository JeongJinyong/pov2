package com.planetorora.base;

/**
 * Created by jinyongjeong on 15. 11. 4..
 */
public class AppConstants {
    public final static boolean DEBUG = true;
    public final static String API_VER = "v1";

    public final static String API_URL = "http://52.69.90.191:54380/api/" + API_VER +"/";


    public final static String ACCOUNT_TYPE = "com.planetorora";
    public final static String USER_DATA = "com.eo.planetorora.user";
    public final static String USER_INFO = "com.eo.planetorora.user.info";
    public final static String FRIEND_VIEW = "com.eo.adb shell setprop log.tag.AccountManagerService VERBOSE\nplanetorora.friend";

    public final static int INTENT_GALLERY = 100;
    public final static int INTENT_IMAGE_CROP = 101;
    public final static int INTENT_IMAGE_CAMERA = 102;

    public final static int INTENT_TAG = 200;
    public final static int INTENT_EMOTION = 201;

    public final static String ANSWER_DATA = "ANSWER_DATA";

    public final static String FONT_REGULAR_KR = "fonts/NotoSansCJKkr-Regular.otf";
    public final static String FONT_THIN_KR = "fonts/NotoSansCJKkr-Regular.otf";
    public final static String FONT_BOLD_KR = "fonts/NotoSansCJKkr-Regular.otf";


}