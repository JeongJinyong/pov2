package com.planetorora.base;

import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;


import com.planetorora.R;

/**
 * Created by berjde on 2015. 12. 17..
 */
public class FriendViewHolder extends RecyclerView.ViewHolder {
    public int userId;
    public ImageView image;
    public TextView name;
    public View mask;
    public View mark;
    public ImageView block;
    public Button action;
    public Button refuse;

    public FriendViewHolder(View itemView) {
        super(itemView);

        image = (ImageView) itemView.findViewById(R.id.item_friend_image);
        name= (TextView) itemView.findViewById(R.id.item_friend_name);
        mask = itemView.findViewById(R.id.item_friend_btn_mask);
        mark = itemView.findViewById(R.id.item_friend_new);
        block = (ImageView) itemView.findViewById(R.id.item_friend_btn_delete);
        action = (Button) itemView.findViewById(R.id.item_friend_button);
        refuse = (Button) itemView.findViewById(R.id.item_friend_refuse_button);
    }

}
