package com.planetorora.base;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by berjde on 2015. 12. 17..
 */
public interface ListSetting<T> {
    public void setUI(JSONObject data, T holder, int position);
    public void setUI(JSONArray data, T holder, int position);
}

