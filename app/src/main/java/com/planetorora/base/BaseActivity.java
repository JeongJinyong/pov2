package com.planetorora.base;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.TextView;

import com.planetorora.R;
import com.planetorora.service.UIAction;
import com.planetorora.service.UserAPI;
import com.planetorora.utility.AccountHelper;
import com.planetorora.utility.ImageHelper;

import org.json.JSONException;
import org.json.JSONObject;

public class BaseActivity extends AppCompatActivity {
    public View mParentView;
    public Toolbar mToolbar;
    public Uri fileUri;
    public ImageButton currentDisplay;
    public ProgressDialog mDialog;
    public JSONObject mAccount = null;
    public String email;
    public String password;
    public String key;
    public String id;
    public UserAPI userAPI = new UserAPI();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAccount = AccountHelper.getUserInfo(this);
        Log.d("test", mAccount.toString());

        try {
            email = mAccount.getString("email");
            password = mAccount.getString("password");
            key = !mAccount.isNull("key") ? mAccount.getString("key") : null;
            id = !mAccount.isNull("id") ? mAccount.getString("id") : null;
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        recycleView(mParentView);
    }

    private void recycleView(View view) {
        if(view != null) {
            Drawable bg = view.getBackground();
            if(bg != null) {
                bg.setCallback(null);
                ((BitmapDrawable)bg).getBitmap().recycle();
                view.setBackgroundDrawable(null);
            }
        }
    }



    public void setBG(int viewId, int resourceId) {
        mParentView = findViewById(viewId);
        BitmapDrawable background = new BitmapDrawable(getResources(), BitmapFactory.decodeResource(getResources(), resourceId));
        background.setTileModeY(Shader.TileMode.REPEAT);
        mParentView.setBackgroundDrawable(background);

    }


    public void setToolbar(String title, OnClickListener action) {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.back_btn));
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((TextView)findViewById(R.id.toolbar_title)).setText(title);

        View btnDone = findViewById(R.id.toolbar_btn_right);

        if(action != null) {
            btnDone.setVisibility(View.VISIBLE);
            btnDone.setOnClickListener(action);
        }
        else{
            btnDone.setVisibility(View.GONE);
            btnDone.setOnClickListener(null);
        }

    }

    public void removeToolbar(){
        getSupportActionBar().hide();
    }

    public void setToolbarRightBtn(int resource, OnClickListener action){
        View btnDone = mToolbar.findViewById(R.id.toolbar_btn_right);
        btnDone.setBackgroundResource(resource);
        btnDone.setVisibility(View.VISIBLE);
        btnDone.setOnClickListener(action);
    }

    public void loadImage(){
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        intent.setData(MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(intent, AppConstants.INTENT_GALLERY);
    }

    public void openCamera(){
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        fileUri = CameraHelper.getOutputMediaFileUri(CameraHelper.MEDIA_TYPE_IMAGE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        startActivityForResult(intent, AppConstants.INTENT_IMAGE_CAMERA);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(requestCode){
            case AppConstants.INTENT_GALLERY :
                if(resultCode == RESULT_OK){
                    Bitmap bitmap = ImageHelper.decodeFile(this, data);
                    Log.d("test", bitmap.getWidth()+" x "+ bitmap.getHeight());
                    currentDisplay.setImageBitmap(bitmap);
                }

                break;
            case AppConstants.INTENT_IMAGE_CAMERA :
                if(resultCode == RESULT_OK){
                    Bitmap bitmap = ImageHelper.decodeFile(fileUri.getPath());
                    Log.d("test", bitmap.getWidth()+" x "+ bitmap.getHeight());
                    //mPostBG.setImageBitmap(bitmap);
                }

                break;
        }



    }

}
