package com.planetorora.photo;

import android.app.ProgressDialog;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.planetorora.R;
import com.planetorora.post.FilterFragment;
import com.planetorora.post.PostActivity;
import com.planetorora.utility.ImageHelper;
import com.planetorora.utility.ImageLoadHelper;

/**
 * A placeholder fragment containing a simple view.
 */
public class GalleryFragment extends Fragment {
    private View mRootView, preSelection;
    private PostActivity mParent;
    private RecyclerView mListView;
    private GridLayoutManager mLayoutManager;
    private GalleryListAdapter mAdapter;
    private ImageLoader loader;
    private  DisplayImageOptions mOptions;

    public static GalleryFragment newInstance() {
        GalleryFragment fragment = new GalleryFragment();
        return fragment;
    }

    @Override
    public void onResume(){
        super.onResume();
        mParent = (PostActivity) getActivity();
        mParent.setToolbarRightBtn(R.drawable.pass_btn, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(thumbnailsSelection > -1 ){
                    FilterFragment fragment = FilterFragment.newInstance();
                    fragment.imagePath = arrPath[thumbnailsSelection];

                    mParent.getSupportFragmentManager().beginTransaction().addToBackStack(null)
                            .replace(R.id.fragment, fragment)
                            .commit();
                }
                else
                    Toast.makeText(mParent, "사진을 선택하세요.", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_gallery, container, false);

        mOptions = new DisplayImageOptions.Builder().
                showImageOnLoading(getResources().getDrawable(R.drawable.default_thumbnail_color)).
                resetViewBeforeLoading(true).
                cacheOnDisk(false).
                cacheInMemory(true).
                bitmapConfig(Bitmap.Config.RGB_565).
                imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2).
                build();

        loader = ImageLoadHelper.getInstance(getActivity());
        setUI();
        return mRootView;
    }

    private void setUI(){
        setListView();
    }

    private void setListView()  {

        mListView = (RecyclerView) mRootView.findViewById(R.id.gallery_view);

        int i = getResources().getDisplayMetrics().densityDpi / 160;

        mLayoutManager = new GridLayoutManager(getActivity(), 4);
        mListView.setLayoutManager(mLayoutManager);

        mAdapter = new GalleryListAdapter(R.layout.item_gallery_grid);
        mListView.setAdapter(mAdapter);
        mListView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);



            }
        });

        new ThumbUriTask().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);

    }



    public int image_column_index;
    public int count;
    public String[] thumbnails;
    public String[] arrPath;
    public int thumbnailsSelection = -1;
    public int pageSize = 40;
    public int current = 0;
    public class GalleryListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        private int mResource;

        private GalleryListAdapter(){}

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(mResource, parent, false);
                return new GalleryViewHolder(view);

        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            final GalleryViewHolder viewHolder = (GalleryViewHolder)holder;
            viewHolder.position = position;
            viewHolder.selection.setVisibility(View.GONE);
            if(thumbnails[position] != null)
                loader.displayImage("file://" + thumbnails[position], viewHolder.image, mOptions);
            else
                viewHolder.image.setImageBitmap(null);

        }


        @Override
        public int getItemCount() {
            if(thumbnails == null) return 0;
            else return thumbnails.length;
        }

        public GalleryListAdapter(int resource){
            this.mResource = resource;

        }


    }



    class GalleryViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener {
        public ImageView image;
        public View selection;
        public int position;
        public GalleryViewHolder(View itemView) {
            super(itemView);

            image = (ImageView) itemView.findViewById(R.id.item_gallery_image);
            selection = (View) itemView.findViewById(R.id.item_gallery_pick);

            itemView.setOnClickListener(this);

        }


        @Override
        public void onClick(View v) {
            if(preSelection != null){
                preSelection.findViewById(R.id.item_gallery_pick).setVisibility(View.GONE);
            }
            thumbnailsSelection = position;
            Log.d("test", position + "");
            preSelection = v;
            v.findViewById(R.id.item_gallery_pick).setVisibility(View.VISIBLE);

        }
    }

    class ThumbUriTask extends AsyncTask<Object, Long, Integer>{

        private int mPosition;


        @Override
        protected Integer doInBackground(Object... params) {

            final String[] columns = { MediaStore.Images.Media.DATA, MediaStore.Images.Media._ID };
            final String orderBy = MediaStore.Images.Media._ID;
            Cursor imagecursor = getActivity().managedQuery(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI, columns, null,
                    null, orderBy);
            image_column_index = imagecursor.getColumnIndex(MediaStore.Images.Media._ID);
            count = imagecursor.getCount();
            thumbnails = new String[count];
            arrPath = new String[count];

            if( pageSize < count ) count = pageSize;

            for (int j = 0; j < count; j++) {
                imagecursor.moveToPosition(j);
                int id = imagecursor.getInt(image_column_index);
                int dataColumnIndex = imagecursor.getColumnIndex(MediaStore.Images.Media.DATA);

                arrPath[j]= imagecursor.getString(dataColumnIndex);
                thumbnails[j] = ImageHelper.getGalleryThumbnail(getActivity(), arrPath[j]);

            }
            return 0;
        }

        @Override
        protected void onPreExecute() {
            ((PostActivity)getActivity()).mDialog = ProgressDialog.show(getActivity(), null, getString(R.string.processing_text));
        }
        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
            ((PostActivity)getActivity()).mDialog.dismiss();
            mAdapter.notifyDataSetChanged();
        }
     }


}
