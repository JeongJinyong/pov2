package com.planetorora.photo;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.planetorora.R;
import com.planetorora.base.CameraHelper;
import com.planetorora.post.FilterFragment;
import com.planetorora.post.ImagePreviewFragment;
import com.planetorora.post.PostActivity;
import com.planetorora.utility.ImageHelper;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.List;

/**
 * Created by berjde on 2015. 12. 17..
 */
public class CameraPreviewFragment extends Fragment {
    private View mRootView;
    private SurfaceView surfaceView;
    private SurfaceHolder surfaceHolder;
    private Camera camera;
    private View mBtnWrap, mBtnCapture, mContent,mBtnRotate,mSelectWrap,mGroupSelection, mBtnSelect, mBtnRetake;
    private ImageView mCapture;
    private boolean isTaking = false;
    private int mTop = 0;
    private int mCameraId = 0;
    private ProgressDialog mDialog;
    private Bitmap mResult;

    public static CameraPreviewFragment newInstance() {
        CameraPreviewFragment fragment = new CameraPreviewFragment();

        return fragment;
    }
    public CameraPreviewFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mRootView =  inflater.inflate(R.layout.fragment_camera_preview, container, false);
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        PostActivity mParent = ((PostActivity) getActivity());
        mParent.removeToolbar();
        setUI();
        setToolbar();
        setPreview();
        return mRootView;

    }

    private void setToolbar() {
        ImageButton leftBtn = (ImageButton) mRootView.findViewById(R.id.toolbar_btn_left);
        ImageButton rightBtn = (ImageButton) mRootView.findViewById(R.id.toolbar_btn_right);
        TextView titleView = (TextView) mRootView.findViewById(R.id.toolbar_title);

        leftBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        rightBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().beginTransaction().addToBackStack(null)
                        .replace(R.id.fragment, ImagePreviewFragment.newInstance())
                        .commit();
            }
        });
        titleView.setText("작성하기");
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    private void setUI() {
        mBtnWrap = mRootView.findViewById(R.id.post_preview_btn_wrap);
        //mGroupSelection = mRootView.findViewById(R.id.post_preview_select_btn_group);
        //mBtnSelect = mRootView.findViewById(R.id.post_preview_btn_select);
        //mBtnRetake = mRootView.findViewById(R.id.post_preview_btn_retake);

        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) mBtnWrap.getLayoutParams();
        int dpi = this.getResources().getDisplayMetrics().densityDpi / 160;
        mTop = 56 * dpi;
        //mTop = (56+getStatusBarHeight()) * dpi;
        Log.d("test", "top size"+ mTop);
        int height = this.getResources().getDisplayMetrics().heightPixels - mTop  - this.getResources().getDisplayMetrics().widthPixels;
        params.height = height;
        mBtnWrap.setLayoutParams(params);

        //mBtnRotate = mRootView.findViewById(R.id.post_preview_btn_rotate);

        mCapture = (ImageView) mRootView.findViewById(R.id.post_preview_capture);
        mBtnCapture = mRootView.findViewById(R.id.post_preview_btn_capture);

//        mBtnRotate.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.d("test", "rotate");
//                if (mCameraId == 0)
//                    mCameraId = 1;
//                else
//                    mCameraId = 0;
//                stopCamera();
//                startCamera();
//                settingCamera();
//            }
//        });

        mBtnCapture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (camera != null && !isTaking) {
                    camera.takePicture(shutterCallback, null, takePicture);
                    isTaking = true;
                }
            }
        });


    }

    public void save(){
        Uri fileUri = CameraHelper.getOutputMediaFileUri(CameraHelper.MEDIA_TYPE_IMAGE);

        try {
            mDialog = ProgressDialog.show(getActivity(), null, getString(R.string.processing_text));
            mResult.compress(Bitmap.CompressFormat.JPEG,80,new FileOutputStream(fileUri.getPath()));
            mDialog.dismiss();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        FilterFragment frag = FilterFragment.newInstance();
        frag.imagePath = fileUri.getPath();
        getActivity().getSupportFragmentManager().beginTransaction().addToBackStack(null)
                .replace(R.id.fragment, frag)
                .commit();

    }


    private final Camera.ShutterCallback shutterCallback = new Camera.ShutterCallback() {
        public void onShutter() {
            AudioManager mgr = (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);
            mgr.playSoundEffect(AudioManager.FLAG_PLAY_SOUND);
        }
    };
    private Camera.PictureCallback takePicture = new Camera.PictureCallback() {

        @Override
        public void onPictureTaken(byte[] data, Camera camera) {

            int width = getResources().getDisplayMetrics().widthPixels;
            int height = getResources().getDisplayMetrics().heightPixels;

            if(data!=null)
                Log.d("test", "taking!!!");
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 2;
            Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length,options);

            Matrix horInvMatrix = new Matrix();
            horInvMatrix.setScale(-1, 1);
            Bitmap resized =  Bitmap.createScaledBitmap(bitmap, height, width, true);

            Bitmap rotated = ImageHelper.rotateImage(resized, mCameraId == 0 ? 90 : -90);
            mResult =Bitmap.createBitmap(rotated, 0, mTop, width, width, mCameraId==0 ? null : horInvMatrix, true);
            Log.d("test", "resized image size" + mResult.getWidth() + "," + mResult.getHeight() + ", " + (height - width) / 2);
            //mCapture.setImageBitmap(mResult);
            //mCapture.setVisibility(View.VISIBLE);
            //mBtnCapture.setVisibility(View.GONE);
            //mBtnRotate.setVisibility(View.GONE);
            //mGroupSelection.setVisibility(View.VISIBLE);
            isTaking = false;
            save();

        }
    };

    private SurfaceHolder.Callback surfaceListener = new SurfaceHolder.Callback() {

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
           stopCamera();
        }

        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            startCamera();
        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width,int height) {
            // TODO Auto-generated method stub
            settingCamera();

        }
    };

    private void settingCamera() {
        Camera.Parameters parameters = camera.getParameters();

        List<Camera.Size> sizes = parameters.getSupportedPreviewSizes();
        Camera.Size optimalSize = sizes.get(0);

        parameters.set("orientation", "portrait");
        camera.setDisplayOrientation(90);
        parameters.setRotation(90);

        if(mCameraId == 0)
            parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);

        parameters.setPreviewSize(optimalSize.width, optimalSize.height);
        Log.d("test", optimalSize.width + ", " + optimalSize.height);

        camera.setParameters(parameters);

        camera.startPreview();
    }

    private void setPreview(){
        surfaceView = (SurfaceView) mRootView.findViewById(R.id.surface_view);
        surfaceHolder = surfaceView.getHolder();
        surfaceHolder.addCallback(surfaceListener);
    }

    private void startCamera(){
        camera = Camera.open(mCameraId);

        try {
            camera.setPreviewDisplay(surfaceHolder);
        }catch(Exception e) {
            e.printStackTrace();
        }
    }
    private void stopCamera(){
        camera.stopPreview();
        camera.release();
    }





}
