package com.planetorora.planet;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import com.planetorora.R;
import com.planetorora.base.BaseActivity;

/**
 * Created by berjde on 16. 3. 12..
 */
public class PlanetarySystemActivity extends BaseActivity {
    public TabLayout tabLayout;
    public ViewPager viewPager;
    public TabPagerAdapter mTapAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tablayout);

        setUI();
        setToolbar("행성계", null);

    }

    private void setUI() {
        setTab();
    }

    private void setTab() {
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        mTapAdapter = new TabPagerAdapter(getSupportFragmentManager());

        viewPager.setAdapter(mTapAdapter);

        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabsFromPagerAdapter(mTapAdapter);

        for(int i=0; i < tabLayout.getTabCount(); i++){
        }
    }
    private class TabPagerAdapter extends FragmentPagerAdapter {

        public TabPagerAdapter(FragmentManager fm) {
            super(fm);
        }
        public String[] tabs = new String[]{"친구", "감성"};
        @Override
        public Fragment getItem(int position) {

            switch (position){
                case 0 :
                    return BlankFragment.newInstance();
                case 1:
                    return BlankFragment.newInstance();

                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return tabs.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {

            return tabs[position];
        }
    }

}
