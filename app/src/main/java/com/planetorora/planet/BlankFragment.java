package com.planetorora.planet;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.planetorora.R;
import com.planetorora.base.ListSetting;
import com.planetorora.post.PostActivity;
import com.planetorora.utility.ImageHelper;
import com.planetorora.utility.ImageLoadHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * A placeholder fragment containing a simple view.
 */
public class BlankFragment extends Fragment {
    private View mRootView;
    public String imagePath;
    public PostActivity mParent;
    private RecyclerView mList;
    private FilterAdapter mAdapter;
    private int mSelected = 0;
    private ImageLoader loader;
    private FeedViewHolder mPreviewSelected;
    public int listType = 1;

    public static BlankFragment newInstance() {
        BlankFragment fragment = new BlankFragment();
        return fragment;
    }

    @Override
    public void onResume(){

        super.onResume();
//        mParent = (PostActivity) getActivity();
//        mParent.getSupportActionBar().show();

//        mParent.setToolbarRightBtn(R.drawable.pass_btn, new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                ImagePreviewFragment frag = ImagePreviewFragment.newInstance();
//                frag.imagePath = imagePath;
//                mParent.getSupportFragmentManager().beginTransaction().addToBackStack(null)
//                        .replace(R.id.fragment, frag )
//                        .commit();
//            }
//        });

        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_list, container, false);
        loader = ImageLoadHelper.getInstance(getActivity());
        //setUI();
        return mRootView;
    }

    private void setUI() {
        setList();
        setImage();

    }

    private void setImage() {

    }

    private void setList() {
        mList = (RecyclerView) mRootView.findViewById(R.id.recycler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mList.setLayoutManager(layoutManager);

        int typeId = -1;
        if(listType == 1){
            typeId = R.layout.item_image_feed_;
        }
        mAdapter = new FilterAdapter(typeId, new ListSetting() {
            @Override
            public void setUI(JSONArray data, Object holder, int position){}
            @Override
            public void setUI(JSONObject data, Object holder, int position){
                FeedViewHolder viewHolder = (FeedViewHolder)holder;
                viewHolder.position = position;
                viewHolder.pressed = false;

                Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.dummy);
                viewHolder.profile.setImageBitmap(ImageHelper.getRoundedCornerBitmap(bm, bm.getWidth() / 2));
                loader.displayImage("drawable://"+R.drawable.dummy_bg, viewHolder.image);

            }
        });

        mList.setAdapter(mAdapter);

        requestData();


    }

    public void requestData() {
        String jsonString = "[{\"id\": 0, \"name\" : \"original\" }," +
                "{\"id\": 1, \"name\" : \"instant\"}," +
                "{\"id\": 2, \"name\" : \"vivid\"}," +
                "{\"id\": 3, \"name\" : \"country\"}," +
                "{\"id\": 4, \"name\" : \"overcast\"}," +
                "{\"id\": 5, \"name\" : \"ansel\"}," +
                "{\"id\": 6, \"name\" : \"holga\"}]";

        try {
            mAdapter.mData = new JSONArray(jsonString);
            mAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public class FeedViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public boolean pressed = false;
        public int position;
        public ImageView profile;
        public ImageView image;
        public FeedViewHolder(View itemView) {
            super(itemView);
            profile = (ImageView) itemView.findViewById(R.id.feed_profile_image);
            image = (ImageView) itemView.findViewById(R.id.feed_post_image);
            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {


        }
    }

    public class FilterAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
        private int mResource;
        public JSONArray mData;
        private ListSetting mSetting;
        public FilterAdapter(){}
        public FilterAdapter(int resource, ListSetting setting){
            this.mResource = resource;
            this.mSetting = setting;
        }
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(mResource, parent, false);
            return new FeedViewHolder(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            try {
                mSetting.setUI(mData.getJSONObject(position), holder, position);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            if(mData == null) return 0;

            return mData.length();
        }


    }

}
