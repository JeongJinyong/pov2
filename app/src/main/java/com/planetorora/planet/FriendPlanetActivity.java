package com.planetorora.planet;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.planetorora.R;
import com.planetorora.base.BaseActivity;

public class FriendPlanetActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_planet);
    }
}
