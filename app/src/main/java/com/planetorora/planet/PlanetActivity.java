package com.planetorora.planet;

import android.animation.Animator;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.planetorora.R;
import com.planetorora.base.AppConstants;
import com.planetorora.base.BaseActivity;
import com.planetorora.emotion.EmotionActivity;
import com.planetorora.friend.FriendActivity;
import com.planetorora.friend.FriendManagerActivity;
import com.planetorora.newsfeed.CurationActivity;
import com.planetorora.newsfeed.FeedFragment;
import com.planetorora.newsfeed.NewsFeedActivity;
import com.planetorora.notification.NotificationActivity;
import com.planetorora.post.PostActivity;
import com.planetorora.post.TestActivity;
import com.planetorora.profile.ProfileActivity;
import com.planetorora.service.UIAction;
import com.planetorora.service.UserAPI;
import com.planetorora.user.SignActivity;
import com.planetorora.utility.AccountHelper;

import org.json.JSONException;
import org.json.JSONObject;

public class PlanetActivity extends BaseActivity {
    public TabLayout tabLayout;
    public ViewPager viewPager;
    public TabPagerAdapter mTapAdapter;
    public ImageView mProfileView;
    public TextView mProfileName;
    public View mUM;
    public ProgressDialog mDialog;
    private int mY;
    private boolean liked = false , visited = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(key == null || mAccount.isNull("data")) {
            refreshUserInfo();
        }
        else{
            setContentView(R.layout.activity_planet);
            setUI();
        }

    }

    public void refreshUserInfo(){
        mDialog = ProgressDialog.show(this,null, getString(R.string.loading_text));

        userAPI.getUser(email, password, new UIAction() {
            @Override
            public void update(boolean success, JSONObject obj) {
                AccountHelper.setData(PlanetActivity.this, AppConstants.USER_DATA, obj.toString());
                try {

                    userAPI.getUserInfo(obj.getString("id"), obj.getString("key"), new UIAction() {
                        @Override
                        public void update(boolean success, JSONObject obj) {
                            AccountHelper.setData(PlanetActivity.this, AppConstants.USER_INFO, obj.toString());
                            mAccount = AccountHelper.getUserInfo(PlanetActivity.this);
                            setContentView(R.layout.activity_planet);
                            setUI();
                            mDialog.dismiss();
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    private void setUI() {
        Toast.makeText(this,mAccount.toString(),Toast.LENGTH_LONG).show();
        setTap();
        setBG(R.id.planet_main_view, R.drawable.planet_bg_2);
        try {
            setProfileImage();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        setProfileEmotion();
        setLikedList();
        setFriendList();
        setUM();
        setActivity();
    }

    public void setToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.main_toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

    }

    private void setActivity() {
        final View likeBtn = findViewById(R.id.planet_activity_liked);
        View astronautBtn = findViewById(R.id.planet_activity_astronaut);
        View planetaryBtn = findViewById(R.id.planet_activity_planetray_system);
        View satelliteBtn = findViewById(R.id.planet_activity_satellite);

        likeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(liked){
                    liked = false;
                    v.setBackgroundResource(R.drawable.btn_activity_normal);
                }
                else{
                    liked = true;
                    v.setBackgroundResource(R.drawable.btn_activity_pressed);
                }
            }
        });

        astronautBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(visited){
                    visited = false;
                    v.setBackgroundResource(R.drawable.btn_activity_normal);
                }
                else{
                    visited = true;
                    v.setBackgroundResource(R.drawable.btn_activity_pressed);
                }
            }
        });

        planetaryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PlanetActivity.this, PlanetarySystemActivity.class);
                startActivity(i);
                overridePendingTransition(0,0);
            }
        });


        satelliteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PlanetActivity.this, CurationActivity.class);
                startActivity(i);
                overridePendingTransition(0,0);
            }
        });
    }



    private void setUM() {
        NestedScrollView scrollView = (NestedScrollView) findViewById(R.id.planet_main_wrap);
        mUM = findViewById(R.id.planet_utility_menu);

        scrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                int moving = scrollY - oldScrollY;
                mY = scrollY;
                if (moving > 0) {
                    if (Math.abs(moving) > 30) {
                        mUM.animate().translationY(0).setDuration(200).setInterpolator(new DecelerateInterpolator()).setListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animation) {
                                mUM.setVisibility(View.GONE);
                                mUM.setTranslationY(-200);
                                mUM.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {

                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animation) {

                            }
                        });
                    }
                } else {
                    if (Math.abs(moving) > 30) {
                        mUM.animate().translationY(0).setDuration(200).setInterpolator(new DecelerateInterpolator()).setListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animation) {
                                mUM.setVisibility(View.GONE);
                                mUM.setTranslationY(30);
                                mUM.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {

                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animation) {

                            }
                        });
                    }

                }


//                if (scrollY > 0) {
//                    mUM.setVisibility(View.VISIBLE);
//                    mUM.animate().alpha(1f).setDuration(200);
//
//
//                } else {
//                    mUM.animate().alpha(0f).setDuration(200);
//                    mUM.setVisibility(View.GONE);
//                }
            }
        });

        View btn_manage_friend = findViewById(R.id.planet_utility_btn_system);
        View btn_notification = findViewById(R.id.planet_utility_btn_notification);
        View btn_newsfeed = findViewById(R.id.planet_utility_btn_newsfeed);
        View btn_post = findViewById(R.id.planet_utility_btn_post);
        View btn_search = findViewById(R.id.main_menu_search);
        View btn_setting = findViewById(R.id.main_menu_setting);
        btn_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AccountHelper.delete(PlanetActivity.this);
                startActivity(new Intent(PlanetActivity.this, SignActivity.class));
                finish();
            }
        });
        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PlanetActivity.this, TestActivity.class);
                startActivity(i);
                overridePendingTransition(0,0);


            }
        });
        btn_manage_friend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PlanetActivity.this, FriendManagerActivity.class);
                startActivity(i);
                overridePendingTransition(0,0);

            }
        });

        btn_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PlanetActivity.this, NotificationActivity.class);
                startActivity(i);
                overridePendingTransition(0,0);
            }
        });

        btn_newsfeed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PlanetActivity.this, NewsFeedActivity.class);
                startActivity(i);
                overridePendingTransition(0,0);
            }
        });

        btn_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PlanetActivity.this, PostActivity.class);
                startActivity(i);
                overridePendingTransition(0, 0);
            }
        });

    }

    private void setLikedList() {
        View view = findViewById(R.id.profile_liked);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PlanetActivity.this, FriendActivity.class);
                i.putExtra(AppConstants.FRIEND_VIEW, FriendActivity.LIKED_LIST);
                startActivity(i);
                overridePendingTransition(0, 0);
            }
        });
    }

    private void setFriendList() {
        View view = findViewById(R.id.profile_friend);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PlanetActivity.this, FriendActivity.class);
                i.putExtra(AppConstants.FRIEND_VIEW, FriendActivity.FRIEND_LIST);
                startActivity(i);
                overridePendingTransition(0, 0);
            }
        });
    }


    private void setProfileEmotion() {
        View view = findViewById(R.id.profile_emotion);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PlanetActivity.this, EmotionActivity.class);
                startActivity(i);
                overridePendingTransition(0, 0);
            }
        });
    }

    private void setProfileImage() throws JSONException {
        Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.dummy);
        mProfileView = (ImageView) findViewById(R.id.profile_image);

        mProfileView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PlanetActivity.this, ProfileActivity.class);
                startActivity(intent);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }
        });

        mProfileName = (TextView) findViewById(R.id.profile_name);
        mProfileName.setText(mAccount.getJSONObject("data").getString("name"));

    }


    private void setTap() {
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        mTapAdapter = new TabPagerAdapter(getSupportFragmentManager());

        viewPager.setAdapter(mTapAdapter);

        tabLayout.setupWithViewPager(viewPager);

        tabLayout.setTabsFromPagerAdapter(mTapAdapter);

        for(int i=0; i < tabLayout.getTabCount(); i++){
            switch(i){
                case 0 :
                    tabLayout.getTabAt(i).setIcon(R.drawable.tab1);
                    break;

                case 1:
                    tabLayout.getTabAt(i).setIcon(R.drawable.tab2);
                    break;

                case 2:
                    tabLayout.getTabAt(i).setIcon(R.drawable.tab3);
                    break;

                case 3:
                    tabLayout.getTabAt(i).setIcon(R.drawable.tab4);
                    break;
            }

        }
    }

    private class TabPagerAdapter extends FragmentPagerAdapter {

        public TabPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            FeedFragment frag = FeedFragment.newInstance();
            frag.listType =0;
            switch (position){
                case 0 :
                case 1:
                case 2:
                case 3:

                default:
                    return frag;
            }
        }

        @Override
        public int getCount() {
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return "";
        }
    }

}
